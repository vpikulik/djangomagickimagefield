
import os

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from magick_image_field.models import MagickFieldImage 

class Command(BaseCommand):
    args = ''
    help = 'Removes cached image thumbnails'

    def handle(self, *args, **options):
        formats = MagickFieldImage.BASE_FORMATS
        formats.update(getattr(settings, 'MAGICK_IMAGE_FIELD_FORMATS', {}))
        thumbnails_dir = getattr(settings, 'MAGICK_IMAGE_FIELD_THUMBNAILS_DIR', MagickFieldImage.THUMBNAILS_DIR)

        for format in formats.keys():
            folder = os.path.join(thumbnails_dir, format)
            self._remove_folder(folder)

    def _remove_folder(self, folder):

        if not os.path.exists(folder):
            return
        
        files = os.listdir(folder)
        for name in files:
            filename = os.path.join(folder, name)
            if os.path.isfile(filename) or os.path.islink(filename):
                os.unlink(filename)
            elif os.path.isdir(filename):
                self._remove_folder(filename)

        os.rmdir(folder)

