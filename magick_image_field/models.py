
import os
from subprocess import check_call, CalledProcessError
from hashlib import md5

from django.db.models.fields.files import FieldFile, FileField
from django.conf import settings

from forms import MagickImageFormField


class MagickFieldImage(FieldFile):

    PREPARE_ORIGIN = u'convert {input} -resize "1024x1024>" {output}'

    BASE_FORMATS = {
        'preview': {
            'command': u'convert {input} -resize 50x50 {output}',
            'format': 'jpg',
        },
        'preview_with_border': {
            'command': u'convert \( -size 50x50 xc:transparent \) {input} -resize "50x50>" -gravity center -compose copy -composite {output}',
            'format': 'png',
        },
        'cutted_preview_with_border': {
            'command': u'convert \( -size 100x100 xc:transparent \) {input} -gravity center -compose copy -composite {output}',
            'format': 'png',
        }
    }

    THUMBNAILS_DIR = os.path.join(settings.MEDIA_ROOT, 'thumbnails')
    THUMBNAILS_URL = settings.MEDIA_URL + u'thumbnails/{format}/{folder1}/{folder2}/{name}.{ext}'

    def __init__(self, *args, **kwargs):
        super(MagickFieldImage, self).__init__(*args, **kwargs)

        self._formats = self.BASE_FORMATS
        self._formats.update(getattr(settings, 'MAGICK_IMAGE_FIELD_FORMATS', {}))
        self._prepare_origin = getattr(settings, 'MAGICK_IMAGE_FIELD_PREPARE_ORIGIN', self.PREPARE_ORIGIN)
        self._thumbnails_dir = getattr(settings, 'MAGICK_IMAGE_FIELD_THUMBNAILS_DIR', self.THUMBNAILS_DIR)
        self._thumbnails_url = getattr(settings, 'MAGICK_IMAGE_FIELD_THUMBNAILS_URL', self.THUMBNAILS_URL)

    class Error(Exception):
        
        def __init__(self, message=None):
            self.message = message

    class DoesNotExist(Exception):
        pass

    def _prepare_folders(self, file_path):
        folder = os.path.dirname(file_path)
        if not os.path.exists(folder):
            os.makedirs(folder)

    def _run_command(self, command, input, output):
        self._prepare_folders(output)
        try:
            check_call(
                command.format(input=input, output=output).encode('utf-8'),
                shell=True
            )
        except CalledProcessError, e:
            raise self.Error(unicode(e))
        return True

    def _get_inner_folders(self):
        name = os.path.basename(self.path)
        (filename, ext) = os.path.splitext(name)
        hashed_name = md5(filename.encode('utf-8')).hexdigest()
        return (hashed_name[12:15], hashed_name[3:6])

    def _get_thumbnail_file_path(self, format):
        name = os.path.basename(self.path)
        (filename, ext) = os.path.splitext(name)
        (folder1, folder2) = self._get_inner_folders()
        return os.path.join(self._thumbnails_dir, format, folder1, folder2, filename + os.path.extsep + self._formats[format]['format'])

    def _prepare_image(self, format):

        if not self:
            return False

        thumbnail_file_path = self._get_thumbnail_file_path(format)

        # test for existing of this file
        if os.path.exists(thumbnail_file_path):
            return True

        if not format in self._formats:
            raise self.DoesNotExist()

        #convert
        self._run_command(self._formats[format]['command'], self.path, thumbnail_file_path)

        return True

    def _get_thumbnail_url(self, format):

        if not self:
            return ''

        name = os.path.basename(self.path)
        (folder1, folder2) = self._get_inner_folders()
        (filename, ext) = os.path.splitext(name)
        self._prepare_image(format)
        return self._thumbnails_url.format(
                    format=format,
                    name=filename,
                    ext=self._formats[format]['format'],
                    folder1=folder1,
                    folder2=folder2,
                )

    def save(self, name, content, save=True):

        super(MagickFieldImage, self).save(name, content, save)
        self._run_command(self._prepare_origin, self.path, self.path)

    def __getattr__(self, key):
        if key.startswith('url__'):
            format = key[5:]
            if format in self._formats:
                self._prepare_image(format)
                return self._get_thumbnail_url(format)
        raise AttributeError()


class MagickImageField(FileField):

    attr_class = MagickFieldImage

    def formfield(self, **kwargs):
        defaults = {
            'form_class': MagickImageFormField,
        }
        defaults.update(kwargs)
        return super(MagickImageField, self).formfield(**defaults)

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^magick_image_field\.models\.MagickImageField"])
except ImportError:
    pass
