from django.forms.fields import FileField
from django.forms.widgets import ClearableFileInput
from django.utils.safestring import mark_safe

class MagickImageWidget(ClearableFileInput):

    def render(self, name, value, attrs=None):
        from models import MagickFieldImage 

        html = super(MagickImageWidget, self).render(name, value, attrs)
        
        if not value:
            return mark_safe(html)

        if isinstance(value, MagickFieldImage):
            return mark_safe(u'''
            <table><tr><td>{html}</td><td><img src="{src}" /></td></tr></table>
            '''.format(html=html, src=value.url__preview_with_border))
        else:
            return mark_safe(html)

class MagickImageFormField(FileField):

    widget = MagickImageWidget