#!/usr/bin/env python

from distutils.core import setup

setup(
    name='DjangoMagickImageField',
    version='0.1',
    description='Django image field with using of command line program',
    author='Vitaly Pikulik',
    author_email='v.pikulik@gmail.com',
    url='https://bitbucket.org/vpikulik/djangomagickimagefield',
    packages=(
        'magick_image_field',
        'magick_image_field.management',
        'magick_image_field.management.commands',
    ),
)